var images = document.querySelectorAll(".gallery-item img");
var accIndex = 0;

document.addEventListener("DOMContentLoaded",function(){
    initGallery();
})

function initGallery()
{
    if(images.length != 0)
    {
        setGallery(0);

        for(var i = 0; i < images.length; i++)
        {
            let index = i;
            images[i].addEventListener("click",function(){
                setGallery(index);
            });
        }
        
        document.querySelector(".left-arrow").addEventListener("click",function(){
            previous();
        });

        document.querySelector(".right-arrow").addEventListener("click",function(){
            next();
        });
    }
}

function setGallery(index)
{
    let mainPic = document.querySelector("#gallery-main img");

    let oldIndex = accIndex;
    accIndex = index;

    arrow_check();

    images.forEach(function(element,index,array){
        element.className="";
    })

    images[index].className = "active";
    mainPic.src = images[index].src;

    if (accIndex>oldIndex) animateCSS("#gallery-main img","zoomInRight");
    else animateCSS("#gallery-main img","zoomInLeft")
    
}

function arrow_check()
{
    if(accIndex == 0)
    {
        document.querySelector(".left-arrow").style.opacity = "0.1";
        document.querySelector(".left-arrow").style.cursor = "not-allowed";
    }
    else if(accIndex == images.length-1)
    {
        document.querySelector(".right-arrow").style.opacity = "0.1";
        document.querySelector(".right-arrow").style.cursor = "not-allowed";
    }
    else
    {
        document.querySelector(".left-arrow").style.opacity = "1";
        document.querySelector(".left-arrow").style.cursor = "pointer"

        document.querySelector(".right-arrow").style.opacity = "1";
        document.querySelector(".right-arrow").style.cursor = "pointer";
    }
        
}

function next()
{
    if(accIndex < images.length-1)
        setGallery(accIndex+1);
}

function previous()
{
    if(accIndex != 0)
        setGallery(accIndex-1);
}

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName, "faster")

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}