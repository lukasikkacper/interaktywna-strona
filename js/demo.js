var demo1_btn = document.getElementById("demo1-btn");
var demo2_btn = document.getElementById("demo2-btn");
var demo3_btn = document.querySelectorAll(".demo3-btn-class");
var demo4_btn = document.getElementById("demo4-btn");

demo1_btn.addEventListener("click",function(event){
    let demo1Element = document.getElementById("demo1");

    demo1Element.innerHTML = "Treść została zmieniona! Brawo!";
});

demo2_btn.addEventListener("click",function(event){
    let demo1Element = document.getElementById("demo2");

    demo1Element.src = "img/2.jpeg";
    demo1Element.alt = "Demo2 Example 2";
});

for(var i = 0; i < demo3_btn.length; i++)
{
    demo3_btn[i].addEventListener("click",function(event){
        let id = event.target.id;
        id = id.replace("3-btn", "");
        demo3(id);
    });
}

demo4_btn.addEventListener("click",function(){
    let demo4Element = document.getElementById("demo4");

    if(demo4_btn.className.includes("on"))
    {
        demo4_btn.innerHTML = "Pokaż";
        demo4_btn.className = "btn off";
        demo4Element.style.display="none";
    }
    else
    {
        demo4_btn.innerHTML = "Ukryj";
        demo4_btn.className = "btn on";
        demo4Element.style.display="block";
    }
});


function demo3(className)
{
    let demo3Element = document.getElementById("demo3");

    demo3Element.className = className;
}