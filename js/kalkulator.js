var inputs = document.querySelectorAll("input");
var select = document.querySelector("select");
var calBtn = document.querySelector("button");
var dynamicCheckbox = document.querySelector("input[type='checkbox']");

var dynamic = false;

dynamicCheckbox.addEventListener("click",function(){
    dynamic = this.checked;
});


select.addEventListener("input",function(){
    if(dynamic)
        cal();
});

inputs.forEach(function(element){
    element.addEventListener("input",function(){
        if(dynamic)
            cal();
    });
});

function cal()
{
    let a = parseInt(document.getElementById("numA").value);
    let b = parseInt(document.getElementById("numB").value);
    let operator = document.getElementById("operator").value;

    let result;
    let stringResult;

    switch(operator){
        case "+":
            result = sum(a,b);
            break;
        case "-":
            result = sub(a,b);
            break
        case "*":
            result = mul(a,b);
            break;
        case "/":
            result = div(a,b);
            break;
    }

    if(isNaN(result) || result === Infinity)
        stringResult = "Nie można wykonać działania";
    else
        stringResult = `${a} ${operator} ${b} = ${result}`;

    

    document.getElementById("operation").innerHTML = stringResult;
}

function sum(a,b)
{
    return a+b;
}

function sub(a,b)
{
    return a-b;
}

function mul(a,b)
{
    return a*b;
}

function div(a,b)
{
    return a/b;
}